# Angular5ToDoApp

This is a simple Angular5 and firebase application for beginners level developers. I actally tried to show how to work with firebase realtime database & 
Angular5. In this application you can add new tasks, If the task is completed you can trick mark on it & it will atometically come to bottom of the list. 
You can also delete tasks.

## Installation

```
git clone https://gitlab.com/saimunhossain/Angular5ToDoApp
cd Angular5ToDoApp
npm install
```
Now you have to configure with firebase database. Go to https://console.firebase.google.com & click on Add Project & give project name & create Project. 
Then It will show firebase dashboard. In the left side click on database. Click on Get Started & then Rules Tab. Your code should be exact like this
```
{
  "rules": {
    ".read": true,
    ".write": true
  }
}
```
Now Hit on Publish button & Dismiss warning message. And back to Project Overview. Click Add firebase to your web app, and it will pop up bunch of codes. 
Copy configuration object from there and paste it in environment.ts file inside environments folder under src folder. Create new property firebase and 
paste the config object there.
```
ng serve

```

## Screenshots
![ScreenShot](/screenshots/screenshot.PNG)

## Permission
If you like the project I will encourage you to download and try to make something different. But If you like it, you can give a star. If you face any problems, 
feel free to email me. My email address given top of my profile. 