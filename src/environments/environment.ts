// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyDoUGFDGEPR4qgG5LJkZatCCehe-Ta44-M",
    authDomain: "todoapp-8ea61.firebaseapp.com",
    databaseURL: "https://todoapp-8ea61.firebaseio.com",
    projectId: "todoapp-8ea61",
    storageBucket: "todoapp-8ea61.appspot.com",
    messagingSenderId: "351173733663"
  }
};
